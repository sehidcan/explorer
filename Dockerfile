FROM node:lts-alpine
RUN apk add --update --no-cache alpine-sdk python

COPY . /

RUN npm i

RUN apk del alpine-sdk python

EXPOSE 3000

ENTRYPOINT ["node"]
